#include "TI_Lib.h" //for TI board methods

/*
 * outputs the error code specific message
 */
void outputErrorMessage(int e) {
    TFT_cls();
    switch (e) {
    case 0:
        break;
    case -1:
        TFT_puts("invalid number range");
        break;
    case -2:
        TFT_puts("stack is full");
        break;
    case -3:
        TFT_puts("invalid result range");
        break;
    case -4:
        TFT_puts("invalid key");
        break;
    case -5:
        TFT_puts("stack is empty");
        break;
    case -6:
        TFT_puts("cannot divide through zero");
        break;
    default:
        TFT_puts("unknown error");
        break;
    }
    if (e != 0) {
        Delay(2500);
    }
    TFT_cls();
}