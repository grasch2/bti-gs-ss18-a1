/**
 ******************************************************************************
 * @file    	main.c
 * @author  	Andrea Grabau
 * @author		Philipp Schwarz
 * @version 	V1.0
 * @date    	2018-03-29
 * @brief   	Main program body
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include < stdio.h > //for sprintf
#include < string.h > // for strcat
#include "TI_Lib.h" //for TI board functions
#include "tft.h" // for tft functions
#include "ErrorOutput.h" //for error output functions
#include "stack.h" //for stack functions
#include "RPN.h" //for math functions
#include "limits.h" //for global constants
//--- For GPIOs -----------------------------
//Include instead of "stm32f4xx.h" for
//compatibility between Simulation and Board
//#include "TI_memory_map.h"

//--- For Touch Keypad ----------------------
#include "keypad.h"
//--- For Timer -----------------------------
//#include "timer.h"

/*
 *A calculator that uses the RPN
 */
int main(void) {
        //Intialize the TI Board and the screen
        Init_TI_Board();
        TFT_Init();

        //Create the touch pad
        Make_Touch_Pad();

        //variables for number input
        int input;
        int number = 0;
        int numberEntered = 0;

        //variables for actions upon button press
        int refvab = 0;
        int e = 0;
        int size = -1;
        int refar[STACKMAX];

        //Superloop
        while (1) {
            input = Get_Touch_Pad_Input();

            switch (input) {
            case 0x30:
                number = number * 10 + 0;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x31:
                number = number * 10 + 1;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x32:
                number = number * 10 + 2;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x33:
                number = number * 10 + 3;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x34:
                number = number * 10 + 4;
                Display_value(number);
                numberEntered = 1;
                break;
            case '5':
                number = number * 10 + 5;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x36:
                number = number * 10 + 6;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x37:
                number = number * 10 + 7;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x38:
                number = number * 10 + 8;
                Display_value(number);
                numberEntered = 1;
                break;
            case 0x39:
                number = number * 10 + 9;
                Display_value(number);
                numberEntered = 1;
                break;
                //Enter
            case 0x20:
                if (MININPUTRANGE <= number && number <= MAXRANGE) {
                    e = push(number);
                    if (e != 0) outputErrorMessage(e);
                } else {
                    outputErrorMessage(-1);
                }
                number = 0;
                numberEntered = 0;
                break;

                // +
            case '+':
                if (numberEntered) {
                    if (MININPUTRANGE <= number && number <= MAXRANGE) {
                        e = push(number);
                        if (e != 0) outputErrorMessage(e);
                    } else if (number != 0) {
                        outputErrorMessage(-1);
                    }
                }

                ADD();
                e = p( & refvab);
                if (e != 0) {
                    outputErrorMessage(e);
                }
                Display_value(refvab);
                number = 0;
                numberEntered = 0;
                break;

                // -
            case 0x2D:
                if (numberEntered) {
                    if (MININPUTRANGE <= number && number <= MAXRANGE) {
                        e = push(number);
                        if (e != 0) outputErrorMessage(e);
                    } else if (number != 0) {
                        outputErrorMessage(-1);
                    }
                }
                SUB();
                e = p( & refvab);
                if (e != 0) {
                    outputErrorMessage(e);
                }
                Display_value(refvab);
                number = 0;
                numberEntered = 0;
                break;

                // *
            case 0x2A:
                if (numberEntered) {
                    if (MININPUTRANGE <= number && number <= MAXRANGE) {
                        e = push(number);
                        if (e != 0) outputErrorMessage(e);
                    } else if (number != 0) {
                        outputErrorMessage(-1);
                    }
                }

                MUL();
                number = 0;
                e = p( & refvab);
                if (e != 0) {
                    outputErrorMessage(e);
                }
                Display_value(refvab);
                numberEntered = 0;
                break;

                // /
            case 0x2F:
                if (numberEntered) {
                    if (MININPUTRANGE <= number && number <= MAXRANGE) {
                        e = push(number);
                        if (e != 0) outputErrorMessage(e);
                    } else if (number != 0) {
                        outputErrorMessage(-1);
                    }
                }
                DIV();
                number = 0;
                e = p( & refvab);
                if (e != 0) {
                    outputErrorMessage(e);
                }
                Display_value(refvab);
                numberEntered = 0;
                break;

                // p
            case 0x70:
                e = p( & refvab);
                if (e != 0) {
                    outputErrorMessage(e);
                }
                Display_value(refvab);
                break;

                // f
            case 0x66:
                e = f(refar, & size);
                if (e != 0) {
                    outputErrorMessage(e);
                } else {
                    char text[256] = "";
                    char temp[20] = "";
                    TFT_cls();
                    for (int i = size - 1; i > -1; i--) {
                        sprintf(temp, "%d ", refar[i]); //copy the current number of the stack to temp
                        strcat(text, temp); //concatinate temp to text array
                    }
                    TFT_cls();
                    TFT_puts(text);
                }
                break;

                // c
            case 0x63:
                c();
                numberEntered = 0;
                break;

                // d
            case 0x64:
                e = d();
                if (e != 0) {
                    outputErrorMessage(e);
                }
                break;

                // r
            case 0x72:
                e = r();
                if (e != 0) {
                    outputErrorMessage(e);
                }
                break;

            default:
                outputErrorMessage(-4);
                break;
            }
        }
        return 0;
    }
    // EOF