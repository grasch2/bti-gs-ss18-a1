#include "limits.h" //for global constants

//stack initialization
int stack[STACKMAX];
int top = 0;

/*
 *puts the adress of the last number on refvab and then deletes it from the stack
 */
int pop(int * refvab) {
    if (top <= 0) {
        return -5;
    } else { * refvab = (stack[top - 1]);
        top--;
        return 0;
    }
}

/*
 *pushes anothern number on the stack
 */
int push(int number) {
    if (top >= STACKMAX) {
        return -2;
    } else {
        stack[top] = number;
        top++;
        return 0;
    }
}

/*
 *returns 1 if stack is empty and 0 if it is not
 */
int isEmpty() {
    if (top < 1) {
        return 1;
    } else {
        return 0;
    }
}

/*
 *returns 1 if stack is full and 0 if not
 */
int isFull() {
    if (top > STACKMAX - 2) {
        return 1;
    } else {
        return 0;
    }
}

/*
 *returns the stack size
 */
int size() {
    return top;
}

/*
 *copies the last entry to refvab
 */
int p(int * refvab) {
    if (top < 1) {
        return -5;
    } else { * refvab = (stack[top - 1]);
        return 0;
    }
}

/*
 *copies the stack to refvab
 */
int f(int * refvab, int * size) {
    if (top > 0) { * size = top;
        for (int i = 0; i < top; i++) {
            refvab[i] = stack[i];
        }
        return 0;
    } else {
        return -5;
    }
}

/*
 *resets the stack
 */
void c() {
    top = 0;
}

/*
 *duplicates the last entry on the stack
 */
int d() {
    if (top >= STACKMAX) {
        return -2;
    } else if (top < 1) {
        return -5;
    } else {
        push(stack[top - 1]);
        return 0;
    }
}

/*
 *switches the last two entries
 */
int r() {
    if (top < 2) return -1;
    else {
        int memory = stack[top - 2]; //memoried second last
        stack[top - 2] = stack[top - 1];
        stack[top - 1] = memory;
        return 0;
    }
}