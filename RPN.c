#include "stack.h" //for stack functions
#include "limits.h" //for global constants
#include "ErrorOutput.h" //for error output functions
#include < limits.h > //for max int

    /*
     *adds the last two numbers on the stack
     */
    void ADD() {
        int error = 0;
        int a;
        error = pop( & a);
        outputErrorMessage(error);
        if (error >= 0) {
            int b;
            error = pop( & b);
            outputErrorMessage(error);
            if (error >= 0) {
                if ((INT_MAX - a) > b) {
                    int res = a + b;
                    push(res);
                } else {
                    outputErrorMessage(-3);
                }
            } else {
                push(a);
            }
        }
    }

/*
 *substractes the last number on the stack from the one before
 */
void SUB() {
    int error = 0;
    int a;
    error = pop( & a);
    outputErrorMessage(error);
    int b;
    error = pop( & b);
    outputErrorMessage(error);
    if ((INT_MIN + a) < b) {
        int res = b - a;
        push(res);
    } else {
        outputErrorMessage(-3);
    }
}

/*
 *multiplies the last two numbers on the stack
 */
void MUL() {
    int error = 0;
    int a;
    error = pop( & a);
    outputErrorMessage(error);
    int b;
    error = pop( & b);
    outputErrorMessage(error);
    if (a <= INT_MAX / b) {
        int res = a * b;
        push(res);
    } else {
        outputErrorMessage(-3);
    }
}

/*
 *divides the last number on the stack through the one before
 */
void DIV() {
    int error = 0;
    int a;
    error = pop( & a);
    outputErrorMessage(error);
    int b;
    error = pop( & b);
    outputErrorMessage(error);
    if (a != 0) {
        int res = b / a;
        if (MINRANGE <= res && res <= MAXRANGE) {
            push(res);
        } else {
            outputErrorMessage(-3);
        }
    } else {
        outputErrorMessage(-6);
    }

}